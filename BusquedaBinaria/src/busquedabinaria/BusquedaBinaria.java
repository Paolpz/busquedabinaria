
package busquedabinaria;
import java.util.Scanner;

public class BusquedaBinaria {

    public static int busquedaBinariaConWhile(String[] arreglo, String busqueda) {
    
    int izquierda = 0, derecha = arreglo.length - 1;
 
    while (izquierda <= derecha) {
        // Calculamos las mitades...
        int indiceDelElementoDelMedio = (int) Math.floor((izquierda + derecha) / 2);
        String elementoDelMedio = arreglo[indiceDelElementoDelMedio];
 
        
        // Primero vamos a comparar y ver si el resultado es negativo, positivo o 0
        int resultadoDeLaComparacion = busqueda.compareTo(elementoDelMedio);
 
        // Si el resultado de la comparación es 0, significa que ambos elementos son iguales
        // y por lo tanto quiere decir que hemos encontrado la búsqueda
        if (resultadoDeLaComparacion == 0) {
            return indiceDelElementoDelMedio;
        }
 
 
        // Si no, entonces vemos si está a la izquierda o derecha
 
        if (resultadoDeLaComparacion < 0) {
            derecha = indiceDelElementoDelMedio - 1;
        } else {
            izquierda = indiceDelElementoDelMedio + 1;
        }
    }
    // Si no se rompió el ciclo ni se regresó el índice, entonces el elemento no
    // existe
    return -1;
}
    public static void main(String[] args) {
        String [][] buscar = { {"Apellido", "Nombre","Telefono"}, {"Alvarado", "Brayan","5734897"}, {"Barranco", "Jaziel","3253265" }, {"Carrasco", "Ximena","3253265" }};


            Scanner teclado = new Scanner(System.in);
            String opc = " ";
            do {
            System.out.println();
            System.out.print("----- Menú Principal -----"
            + "\nA. Ver la Lista"
            + "\nB. Buscar por su Apellido"
            + "\nC. Salir"
            + "\n");

            System.out.println("Seleccione que accion desea realizar");
            opc = teclado.nextLine();

            switch (opc) {
            case "a":
            case "A":
                String cad="";
                for(int i=0;i<4;i++){
                for(int j=0;j<3;j++){
                cad+=buscar[i][j]+" ";
                }
                cad+="\n";
                }
                System.out.println(cad);
            break;

            case "b":
                    case "B": 

                        String[] arreglo = { "Nombre","Alvarado", "Barranco", "Carrasco" };

                    String busqueda = "Barranco";
                        int indiceDelElementoBuscado = busquedaBinariaConWhile(arreglo, busqueda);

                        for(int i=0;i<4;i++){
                                if(buscar[i][0].equals(busqueda)){
                                     System.out.println( busqueda + " se encuentra en el index  " + indiceDelElementoBuscado+"\n"+ buscar[i][0]+" "+buscar[i][1]+" "+ buscar[i][2]);
                                }
                        }    

                            break;


                    case "c":
                    case "C":
                        System.out.println();
                        System.out.println("Gracias por usar la aplicación. Vuelva Pronto");
            }

            }while(!opc.equalsIgnoreCase("C"));



}
    }
    
